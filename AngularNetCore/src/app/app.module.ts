import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router"
import { HttpModule } from "@angular/http"
import { AppComponent } from './app.component';
import { NavbarComponent } from "./maincomponents/navbar/navbar.component";
import { SidebarComponent } from './maincomponents/sidebar/sidebar.component';


const routes: Routes = [
    {
      path : '',
      redirectTo: 'home',
      pathMatch : 'full'
    },
    {
      path: 'home',
      loadChildren : './home/home.module#HomeModule'
    }
];


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
