import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from "@angular/http"
import { FormsModule } from "@angular/forms"


import { HomeRoutingModule } from './home-routing.module';
import { HomeIndexComponent } from './home-index/home-index.component';
import { PageHeaderComponent } from '../maincomponents/page-header/page-header.component';


@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    HomeRoutingModule
  ],
  declarations: [HomeIndexComponent,PageHeaderComponent]
})
export class HomeModule { }
