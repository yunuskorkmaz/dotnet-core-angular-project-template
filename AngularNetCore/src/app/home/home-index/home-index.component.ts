import { Component, OnInit } from '@angular/core';
import { Http } from "@angular/http"



@Component({
  selector: 'app-home-index',
  templateUrl: './home-index.component.html',
  styleUrls: ['./home-index.component.css']
})
export class HomeIndexComponent implements OnInit {

  public values : string[] = [];

  constructor(private http: Http) {
    this.loadInitData();
  }

  ngOnInit() {
  }


  loadInitData() {
    this.http.get("api/values").subscribe(values => {
      this.values = values.json() as string[];
    });
  }

}
