# .Net Core Web App With Angular

This project is a project development template with .NetCore and Angular

# Client

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.3. and [.Net Core](https://dotnet.github.io/) version 2.0

## Tools, Practices and Technologies

* Cross-Platform (Windows, Linux, macOS)
* Visual Studio 2017
* Visual Studio Code
* .NET Core 2.0
* SPA (Single Page Application)
* Angular 6.1.0
* Typescript 2.9.2
* HTML5
* CSS3
* UIkit
* Dependency Injection

## Instruction
Run Command for install node modules.

    npm install
Run Command  for install nuget package

    dotnet restore

## Development server

Run for a build angular project and .netcore project.

     dotnet build
Run for a start dev server

    dotnet run

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
and  [.Net Core README](https://github.com/dotnet/core/blob/master/README.md).


### Owner

[Yunus Korkmaz](https://github.com/yunuskorkmaz)
